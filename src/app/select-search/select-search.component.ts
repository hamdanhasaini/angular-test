import { Component } from '@angular/core';

@Component({
  selector: 'app-select-search',
  templateUrl: './select-search.component.html',
  styleUrls: ['./select-search.component.css']
})
export class SelectSearchComponent {
  options = [
    { value: 'option1', label: 'Option 1' },
    { value: 'option2', label: 'Option 2' },
    { value: 'option3', label: 'Option 3' },
    // Add more options as needed
  ];

  filteredOptions: any[] = this.options;
  searchText: string = '';
  selectedOption: string = '';

  // Function to filter options based on search text
  filterOptions(): void {
    this.filteredOptions = this.options.filter(option =>
      option.label.toLowerCase().includes(this.searchText.toLowerCase())
    );
  }
}
