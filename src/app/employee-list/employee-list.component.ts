import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee.model';

interface DynamicObject {
  [key: string]: any;
}

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[] = [];
  filteredEmployees: Employee[] = [];
  searchTerm: string = '';
  currentPage: number = 1;
  pageSize: number = 10;
  sortField: string = '';
  sortDirection: string = 'asc';
  successToast: boolean = false;
  errorToast: boolean = false;
  updateToast: boolean = false;
  deleteToast: boolean = false;
  addEmployee: boolean = false;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.employeeService.getEmployees().subscribe(data => {
      this.employees = data;
      this.filteredEmployees = this.employees.slice();
    });
  }

  filterEmployees() {
    if (!this.searchTerm) {
      this.filteredEmployees = this.employees.slice();
    } else {
      this.filteredEmployees = this.employees.filter(employee =>
        this.matchesSearchCriteria(employee, this.searchTerm.toLowerCase())
      );
    }
    this.currentPage = 1; // Reset to the first page when searching
  }

  matchesSearchCriteria(employee: Employee, searchTerm: string): boolean {
    // Define the columns you want to search in
    const searchableColumns = ['username', 'email', 'firstName', 'lastName'];

    // Check if the search term exists in any of the searchable columns
    return searchableColumns.some(column =>
      (employee as DynamicObject)[column].toLowerCase().includes(searchTerm)
    );
  }



  sort(field: string) {
    if (field === this.sortField) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortField = field;
      this.sortDirection = 'asc';
    }

    // Create a new sorted array instead of sorting in place
    this.filteredEmployees = [...this.filteredEmployees].sort((a, b) => {
      const valueA = (a as any)[this.sortField];
      const valueB = (b as any)[this.sortField];

      if (valueA < valueB) {
        return this.sortDirection === 'asc' ? -1 : 1;
      } else if (valueA > valueB) {
        return this.sortDirection === 'asc' ? 1 : -1;
      } else {
        return 0;
      }
    });
  }
  options: any[] = [
    { value: 'VPAccounting', label: 'VP Accounting' },
    { value: 'EngineerII', label: 'Engineer II' },
    { value: 'HumanResourcesAssistantII', label: 'Human Resources Assistant II' },
    { value: 'PaymentAdjustmentCoordinator', label: 'Payment Adjustment Coordinator' },
    { value: 'DataCoordinator', label: 'Data Coordinator' },
    { value: 'CivilEngineer', label: 'Civil Engineer' },
  ];

  filteredOptions: any[] = this.options;
  searchText: string = '';
  selectedOption: string = '';

  formData = {
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    basicSalary: '',
    status: '',
    group: '',
    description: '',
  };

  onSubmit(): void {
    console.log('submit')
    try {
      if (this.formData.username.trim() === '' ||
      this.formData.firstName.trim() === '' ||
      this.formData.lastName.trim() === '' ||
      this.formData.email.trim() === '' ||
      this.formData.birthDate.trim() === '' ||
      this.formData.basicSalary.trim() === '' ||
      this.formData.status.trim() === '' ||
      this.formData.group.trim() === '' ||
      this.formData.description.trim() === ''
    ) {
      this.errorToast = true;
      setTimeout(() => {
        this.errorToast = false;
      }, 3000);
    } else {
      this.successToast = true;
      
      setTimeout(() => {
        this.successToast = false;
        for (const key in this.formData) {
          if (this.formData.hasOwnProperty(key)) {
            this.formData[key as keyof typeof this.formData] = null as any;
          }
          this.addEmployeeTogle()
        }
      
      }, 3000);
    }
    } catch (error) {
      this.errorToast = true;
      setTimeout(() => {
        this.errorToast = false;
      }, 3000);
    }
    
  }
  filterOptions(): void {
    this.filteredOptions = this.options.filter(option =>
      option.label.toLowerCase().includes(this.searchText.toLowerCase())
    );
  }
  selectStatus(status: string | 'active') {
    this.formData.status = status;
  }
  selectGroup(status: string | 'active') {
    this.formData.group = status;
  }


  addEmployeeTogle(): void {
    this.selectedItem = false;
    this.addEmployee = !this.addEmployee;
  }

  public onAction(type: boolean): void {
    if (type) {
      this.updateToast = true;
      setTimeout(() => {
        this.updateToast = false;
      }, 3000);
    }
    else {
      this.deleteToast = true;
      setTimeout(() => {
        this.deleteToast = false;
      }, 3000);
    }

  }

  toRupiah(angka: number) {
    return "Rp. " + Intl.NumberFormat('id').format(angka);
  }
  public selectedItem: any = null;

  public onItemClick(item: any): void {
    this.addEmployee = false;
    this.selectedItem = item;
  }

  public resetItem(): void {
    this.selectedItem = null;
  }
  public isStatusEqual(item: any, value: string): boolean {
    return item === value;
  }


  get pagedEmployees(): Employee[] {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.filteredEmployees.slice(startIndex, endIndex);
  }

  get pages(): number[] {
    const pageCount = Math.ceil(this.filteredEmployees.length / this.pageSize);
    return Array.from({ length: pageCount }, (_, i) => i + 1);
  }

  changePage(page: number) {
    if (page >= 1 && page <= this.pages.length) {
      this.currentPage = page;
    }
  }
}
