import { Component, OnInit } from '@angular/core';
// import { LocalStorageService } from './local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'angular-test-app';
  session: any;
  isLoggedIn: boolean = false;

  // saveData(){
  //   // let data = { isLoggedIn: false }
  //   // localStorage.setItem('session',JSON.stringify(data));
  //   localStorage.setItem('isLoggedIn','true');
  // }
  // getData(){
  //   let data = localStorage.getItem('session');
  //   if (data) {
  //     data = JSON.parse(data);
  //     console.log(data);
  //     // if (data.isLoggedIn) {

  //     // }
  //   }
  // }
  // sharedValue: string = 'String';
  // constructor(private localStorageService= LocalStorageService) {}

  logOut()
  {
    localStorage.clear();
    window.location.href = "/login"
  }
  ngOnInit(): void {
    // this.getData();
    const isLoggedIn = localStorage.getItem('isLoggedIn');
    this.isLoggedIn = isLoggedIn === 'true';
  }
    
  //   // Load the value from localStorage
  //   this.sharedValue = this.localStorageService.getValue('sharedValue');

  //   // Check if the value needs to be updated
  //   if (!this.sharedValue) {
  //     // Value not found in localStorage, set an initial value
  //     this.sharedValue = 'Initial Value';
  //     // Update localStorage with the initial value
  //     this.localStorageService.setValue('sharedValue', this.sharedValue);
  //   }
  // }

}

